use vecmath;
use plain_theme::*;

pub type Area = Option<Rect>;

#[derive(Copy, Clone)]
pub enum LineShape {
    Square,
    Round
}

pub type Transform = vecmath::Matrix2x3<i64>;

pub trait Renderer {
    fn clear(&mut self, area: Area, color: Color);
    fn polygon(&mut self, area: Area, polygon: &[[i64; 2]], color: Color);
    fn line(
        &mut self,
        area: Area,
        line: [[i64; 2]; 2],
        color: Color,
        radius: u64,
        shape: LineShape,
    );
    fn rect(
        &mut self,
        area: Area,
        rect: Rect,
        color: Color,
        shape: RectShape,
        border_color: Color,
        border_radius: u64,
    );
}

pub trait Size {
    fn size(&self) -> [u64; 2];
}

pub trait TextureRenderer<T: Size>: Renderer {
    fn texture(
        &mut self,
        area: Area,
        rect: Rect,
        texture: &T,
    );
}

pub trait TextSize {
    fn text_size(&mut self, text: &str) -> [u64; 2];
}

pub trait TextRenderer<T: TextSize>: Renderer {
    fn text(
        &mut self,
        area: Area,
        rect: Rect,
        font: &mut T,
        text: &str,
        color: Color,
    );
}

pub fn offset_polygon(polygon: &[[i64; 2]], offset: [i64; 2]) -> Vec<[i64; 2]> {
    let mut new_polygon = Vec::with_capacity(polygon.len());

    for point in polygon {
        new_polygon.push(vecmath::vec2_add(*point, offset));
    }

    new_polygon
}

pub fn offset_line(line: [[i64; 2]; 2], offset: [i64; 2]) -> [[i64; 2]; 2] {
    [vecmath::vec2_add(line[0], offset), vecmath::vec2_add(line[1], offset)]
}

pub fn offset_area(area: Area, offset: [i64; 2]) -> Area {
    match area {
        Some(rect) => Some(offset_rect(rect, offset)),
        None => None,
    }
}

pub fn combine_areas(area_0: Area, area_1: Area) -> Area {
    match (area_0, area_1) {
        (None, None) => None,
        (None, Some(rect_1)) => Some(rect_1),
        (Some(rect_0), None) => Some(rect_0),
        (Some(rect_0), Some(rect_1)) => Some(rect_intersection(rect_0, rect_1)),
    }
}

pub fn point_in_area(point: [i64; 2], area: Area) -> bool {
    match area {
        Some(rect) => point_in_rect(point, rect),
        None => true,
    }
}
